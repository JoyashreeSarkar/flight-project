import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Flights from '@/components/pages/Flights'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      redirect: {path: "flights"}
    },
    {
      path: '/flights',
      name: 'flights',
      component: Flights
    },

  ],
  mode: "history"
})
